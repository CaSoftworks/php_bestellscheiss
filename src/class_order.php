<?php 
/***
 * Usually I`d load an existing one from db on construction but we
 * dont need to output them anywhere so loading them is not required.
 * Also I`d like to put the queries and the PDO object in an extra class,
 * but its not required for this project...
 */
class cOrder {
    /* @var $db PDO */
    private $db = null;

    /* @var $stmt_select_all_users PDOStatement */
    private $stmt_insert_order = null;

    /* @var $persNumber String */
    private $persNumber = null;

    /* @var $drinkType int */
    private $drinkType = null;

    /* @var $withMilk bool */
    private $withMilk = null;

    /* @var $withSugar bool */
    private $withSugar = null;

    ////////////////////////////////////////////////////////////////////////////////////

    public function __construct() {
        $host       = "localhost";
        $database   = "test";
        $user       = "root";
        $password   = "Dx6FHRWu";

        // connect to db
        $this->db = new PDO( "mysql:host=".$host.";dbname=".$database, $user, $password );

        if( $this->db === null ) {
            exit( "Cannot connect to database!" );
        }

        // set error mode
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // install db if table doesnt exist
        $results = $this->db->query("SHOW TABLES LIKE 'orders'");
        $dbTableExists = !$results ? false : $results->rowCount() > 0;

        if($dbTableExists === false) {
            $createTableQuery ="CREATE table orders(
            id INT( 255 ) NOT NULL AUTO_INCREMENT PRIMARY KEY,
            pers_number VARCHAR( 10 ) NOT NULL, 
            drink_type INT( 1 ) NOT NULL,
            with_milk  TINYINT( 1 ) NOT NULL, 
            with_sugar TINYINT( 1 ) NOT NULL);";
            $this->db->exec($createTableQuery);
        }

        // init statement
        $this->stmt_insert_order = $this->db->prepare( "INSERT INTO orders (pers_number, drink_type, with_milk, with_sugar) VALUES (:pers_number, :drink_type, :with_milk, :with_sugar)" );
    }

    ////////////////////////////////////////////////////////////////////////////////////

    public function getPersNumber( ){ return $this->persNumber; }
    public function getDrinkType( ){ return $this->drinkType; }
    public function getWithMilk( ){ return $this->withMilk; }
    public function getWithSugar( ){ return $this->withSugar; }

    ////////////////////////////////////////////////////////////////////////////////////

    public function setPersNumber( $val ){ $this->persNumber = $val; }
    public function setDrinkType( $val ){ $this->drinkType = $val; }
    public function setWithMilk( $val ){ $this->withMilk = $val; }
    public function setWithSugar( $val ){ $this->withSugar = $val; }

    ////////////////////////////////////////////////////////////////////////////////////

    public function save() {
        $this->stmt_insert_order->bindParam( ":pers_number", $this->persNumber, PDO::PARAM_STR );
        $this->stmt_insert_order->bindParam( ":drink_type", $this->drinkType, PDO::PARAM_INT );
        $this->stmt_insert_order->bindParam( ":with_milk", $this->withMilk, PDO::PARAM_BOOL );
        $this->stmt_insert_order->bindParam( ":with_sugar", $this->withSugar, PDO::PARAM_BOOL );
        return $this->stmt_insert_order->execute( );
    }

    ////////////////////////////////////////////////////////////////////////////////////
};