<?php
require_once "class_order.php";
/**
 * Created by PhpStorm.
 * User: sc
 * Date: 30/05/2017
 * Time: 10:12
 */
    $ordered = isset($_GET["ordered"]);
    $persNumber = isset($_GET["personellNumber"])? $_GET["personellNumber"] : null;
    $drinkType = isset($_GET["drinkType"])? intval($_GET["drinkType"]) : null;
    $withMilk = isset($_GET["withMilk"])? $_GET["withMilk"] === "on" : false;
    $withSugar = isset($_GET["withSugar"])? $_GET["withSugar"] === "on" : false;
    $error = false;

    if($ordered) {
        $error = strlen($persNumber) < 1 && strlen($persNumber) > 10;

        if(!$error) {
            $newOrder = new cOrder();
            $newOrder->setPersNumber($persNumber);
            $newOrder->setDrinkType($drinkType);
            $newOrder->setWithMilk($withMilk);
            $newOrder->setWithSugar($withSugar);
            $newOrder->save();
        }
    }
?>
    <html >
    <head >
        <title >Getr&auml;nkebestellautomat</title>
        <link rel="stylesheet" href="./styles.css" >
        <link rel="icon" href="favicon.ico" >
    </head>
    <?php if (!$ordered || ($ordered && $error)): ?>
        <body >
        <main id="body-main" >
            <!-- Window style? -->

            <header id="window-head" >
                <p >Getr&auml;nkebestellung</p>
            </header>
            <main id="window-main" >
                <span class="space" ></span><br >
                <form method="GET" action="index.php" >
                    <input type="checkbox" checked style="display: none;" name="ordered" >
                    <?php if ($error): ?>
                        <strong class="warning" >Ihre Personalnummer darf nicht leer oder l&auml;nger als 10 Zeichen sein!</strong>
                        <span class="space" ></span>
                    <?php endif; ?>
                    <label >Geben Sie Ihre Personal-ID ein: <input type="text" name="personellNumber" ></label>
                    <span class="space" ></span>
                    <strong class="warning" >Sie k&ouml;nnen nur 1 Getr&auml;nk je Aufruf bestellen!</strong>
                    <span class="space" ></span>
                    <label class="float-left" >Kaffee <input type="radio" name="drinkType" value="1" checked ></label><label >mit Zucker <input type="checkbox" name="withSugar" ></label><label >&nbsp;&nbsp;mit Milch <input type="checkbox" name="withMilk" ></label>
                    <span class="nospace" ></span>
                    <label class="float-left" >Tee&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="radio" name="drinkType" value="2" ></label><label >mit Zucker <input type="checkbox" name="withSugar" ></label><label >&nbsp;&nbsp;mit Milch <input type="checkbox" name="withMilk" ></label>
                    <span class="nospace" ></span>
                    <label class="float-left" >Kakao <input type="radio" name="drinkType" value="3" ></label>
                    <br ><br >
                    <input type="submit" value="bestellen" >
                </form>
            </main>

        </main>
        </body>
    <?php else: ?>
        <body >
        <main id="body-main" >
            <!-- Window style? -->

            <header id="window-head" >
                <p >Getr&auml;nkebestellung</p>
            </header>
            <main id="window-main" >
                <strong id="ok" >Ihre Bestellung wurde aufgegeben!</strong>
                <span class="space" ></span>
                <a href="index.php" class="no-blue" ><button >Zur&uuml;ck</button></a>
            </main>
        </body>
    <?php endif; ?>

    </html>